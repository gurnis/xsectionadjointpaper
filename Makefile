BASENAME = xsection_adjoint

all: pdf

pdf::
	latexmk --pdf ${BASENAME}

ps::
	latexmk --ps ${BASENAME}

clean::
	latexmk -c ${BASENAME}


